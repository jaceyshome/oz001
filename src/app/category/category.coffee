angular.module("category", [
		"utilService",
		"modalService",
])
.directive "category",()->
		restrict: "A"

		scope:{
				resource: '='
		}

		replace: true

		template:
				'<div class="category">
					<div class="headingContainer">
						<h2>{{resource.category}}</h2>
					</div>
					<ul class=" listContainer">
						<li data-ng-repeat="web in resource.webs">
							<a	class="webLink"	data-ng-href="{{web.link}}"
									target="_blank"
									href=""
									title="click to open a new window for {{web.link}}"
								>{{web.name}}</a>
						</li>
					</ul>
				</div>'

		link: ($scope, $element, $attrs) ->

				init = () ->

				init()
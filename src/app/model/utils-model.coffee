angular.module("Utils", [])
.factory "Utils", () ->

		Utils = {
				loadingPercent : 0
#				get image by screen size
				getImage: (uri)->
						return uri

				checkLoadingProcess:(completePercentage)->
						@loadingPercent += completePercentage
						return (completePercentage >= 100)
		}

		return Utils
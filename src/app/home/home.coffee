angular.module("myApp.home", [
		"ui.state"
		"Utils"
		"dataService"
		"googleSearchBar"
		"iconButton"
		"category"
]).config(($stateProvider) ->
		$stateProvider.state "home",
				url: "/home"
				views:
						main:
								controller: "HomeCtrl"
								templateUrl: "home/home.tpl.html"
				data:
						pageTitle: 'OZ365'

).controller "HomeCtrl", HomeCtrl = (
		$scope,
		dataService
	) ->
		#---private variables
		loadProcess = 0

		#---private functions
		init = ()->
				initLoadingData(handleLoadingCompleteCallback)

		initLoadingData = (callback)->
				dataService.get((result)->
						console.log result
						$scope.categories = result.categories
						$scope.categoryWebs = result.categoryWebs
						$scope.quickLinks = result.quickLinks
						handleLoadingComplete(100, callback)
				)

		restructureCategories = ()->
				return unless angular.isDefined($scope.categories)
				return unless angular.isDefined($scope.categoryWebs)

				for category in $scope.categories
						category.webs = []
						for web in $scope.categoryWebs
								if category.id is web.categoryId
										category.webs.push web

		checkLoadingProcess = (completePercentage)->
				loadProcess += completePercentage
				return (loadProcess >= 100)

		#---scope functions
		$scope.getQuickLinkThumb = (url)->
				return url

		#---handle functions
		handleLoadingCompleteCallback = ()->
				restructureCategories()

		handleLoadingComplete = (percentage,callback)->
				if checkLoadingProcess(percentage)
						if callback then callback()

		init()
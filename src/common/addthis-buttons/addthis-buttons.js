angular.module("addthisButtons", []).directive("addthisButtons", function($location) {
	return {
		restrict: "A",
		scope: {
			moduleName: "=",
			active: "="
		},

		transclude: true,

		link:function ($scope, $element){
			// ---------------------------------------------------------------------------- $scope Variables
			$scope.$location = $location;

			// ---------------------------------------------------------------------------- Private Variables
			var template =	"<ul>" +
				"<li class='liEmail'>" +
				"<a id='addThisButtonEmail' addthis:title='{{getTitle()}}' addthis:url='{{$location.absUrl()}}' class='addthis_button_email' >EMAIL</a>" +
				"</li>" +
				"<li class='liShare' >" +
				"<a id='addThisButtonFacebook'  addthis:url='{{$location.absUrl()}}' class='addthis_button_facebook'  >SHARE</a>" +
				"</li>" +
				"<li class='liTweet'>" +
				"<a id='addThisButtonTwitter' addthis:url='{{$location.absUrl()}}' class='addthis_button_twitter'  >TWEET</a>" +
				"</li>" +
				"</ul>";

			// ---------------------------------------------------------------------------- Init
			var init = function init () {
				$scope.active = true;
				addWatchers();
				addListeners();
			};

			// ---------------------------------------------------------------------------- $scope Functions
			$scope.getTitle = function () {
				return "OZ 365 " + $scope.moduleName;
			};

			// ---------------------------------------------------------------------------- Private Functions
			var addWatchers = function addListeners () {
				$scope.$watch('active', handleActiveChange);
				$scope.$watch('moduleName', handleModuleNameChange);
			};

			var addListeners = function addListeners () {
				$scope.$on('$destroy', removeListeners);
			};

			var removeListeners = function removeListeners () {
			};

			var addToolbox = function addToolbox () {
				console.log("$scope.moduleName: ",  $scope.moduleName);

				if ($element.children().length !== 0) {
					removeToolbox();
				}

				$element.attr("id", "llsToolbox");
				$element.append($compile(template)($scope));

				setTimeout(function() {
					addthis.toolbox("#llsToolbox");

					/*$element.find('#addThisButtonEmail').attr("href", "javascript:void(0)");
					$element.find('#addThisButtonFacebook').attr("href", "javascript:void(0)");
					$element.find('#addThisButtonTwitter').attr("href", "javascript:void(0)");
					$element.find('#buttonPrint').attr("href", "javascript:void(0)");

					$element.find("#addThisButtonEmail").on('click', handleClickShareEmail);*/
				}, 1);
			};

			var removeToolbox = function removeToolbox () {
				$element.attr("id", "");

				$element.find("#addThisButtonEmail").off('click', handleClickShareEmail);

				$element[0].innerHTML = "";
			};

			// ---------------------------------------------------------------------------- Handler Functions
			var handleActiveChange = function handleActiveChange () {
				if (canAddToolbox()) {
					addToolbox();
				} else {
					removeToolbox();
				}
			};

			var handleModuleNameChange = function handleModuleNameChange () {
				if (canAddToolbox()) {
					addToolbox();
				} else {
					removeToolbox();
				}
			};

			var handleClickShareEmail = function handleClickShareEmail() {
				if ($scope.onShareEmail) {
					$scope.onShareEmail();
				}
			};

			// ---------------------------------------------------------------------------- Helper Functions
			var canAddToolbox = function () {
				return true;
//				return $scope.moduleName != null && $scope.active === true;
			};

			// ---------------------------------------------------------------------------- init()
			init();
		}

		};
	});

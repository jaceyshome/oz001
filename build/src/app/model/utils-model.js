angular.module("Utils", []).factory("Utils", function() {
  var Utils;
  Utils = {
    loadingPercent: 0,
    getImage: function(uri) {
      return uri;
    },
    checkLoadingProcess: function(completePercentage) {
      this.loadingPercent += completePercentage;
      return completePercentage >= 100;
    }
  };
  return Utils;
});

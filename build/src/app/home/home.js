var HomeCtrl;

angular.module("myApp.home", ["ui.state", "Utils", "dataService", "googleSearchBar", "iconButton", "category"]).config(function($stateProvider) {
  return $stateProvider.state("home", {
    url: "/home",
    views: {
      main: {
        controller: "HomeCtrl",
        templateUrl: "home/home.tpl.html"
      }
    },
    data: {
      pageTitle: 'OZ365'
    }
  });
}).controller("HomeCtrl", HomeCtrl = function($scope, dataService) {
  var checkLoadingProcess, handleLoadingComplete, handleLoadingCompleteCallback, init, initLoadingData, loadProcess, restructureCategories;
  loadProcess = 0;
  init = function() {
    return initLoadingData(handleLoadingCompleteCallback);
  };
  initLoadingData = function(callback) {
    return dataService.get(function(result) {
      console.log(result);
      $scope.categories = result.categories;
      $scope.categoryWebs = result.categoryWebs;
      $scope.quickLinks = result.quickLinks;
      return handleLoadingComplete(100, callback);
    });
  };
  restructureCategories = function() {
    var category, web, _i, _len, _ref, _results;
    if (!angular.isDefined($scope.categories)) {
      return;
    }
    if (!angular.isDefined($scope.categoryWebs)) {
      return;
    }
    _ref = $scope.categories;
    _results = [];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      category = _ref[_i];
      category.webs = [];
      _results.push((function() {
        var _j, _len1, _ref1, _results1;
        _ref1 = $scope.categoryWebs;
        _results1 = [];
        for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
          web = _ref1[_j];
          if (category.id === web.categoryId) {
            _results1.push(category.webs.push(web));
          } else {
            _results1.push(void 0);
          }
        }
        return _results1;
      })());
    }
    return _results;
  };
  checkLoadingProcess = function(completePercentage) {
    loadProcess += completePercentage;
    return loadProcess >= 100;
  };
  $scope.getQuickLinkThumb = function(url) {
    return url;
  };
  handleLoadingCompleteCallback = function() {
    return restructureCategories();
  };
  handleLoadingComplete = function(percentage, callback) {
    if (checkLoadingProcess(percentage)) {
      if (callback) {
        return callback();
      }
    }
  };
  return init();
});

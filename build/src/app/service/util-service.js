angular.module("utilService", []).factory("utilService", function() {
  var utilService;
  utilService = {
    closeAllPopup: void 0,
    openEditPanel: void 0
  };
  return utilService;
});

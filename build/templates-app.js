angular.module('templates-app', ['home/home.tpl.html']);

angular.module("home/home.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("home/home.tpl.html",
    "<div data-ng-cloak class=\"homePage\">\n" +
    "	<div data-addthis-buttons></div>\n" +
    "	<div data-google-search-bar></div>\n" +
    "\n" +
    "	<div class=\"inner-container quickLinksContainer\" >\n" +
    "		<div class=\"row inner-block\">\n" +
    "			<ul >\n" +
    "				<li class=\"col-md-3 quickLinkHolder \" data-ng-repeat=\"link in quickLinks | orderBy: 'name': false\">\n" +
    "					<div data-icon-button data-resource=\"link\"  ></div>\n" +
    "				</li>\n" +
    "			</ul>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "\n" +
    "	<div class=\"inner-container categoryWebsContainer\">\n" +
    "		<div class=\"row inner-block\">\n" +
    "			<ul class=\"categoryContent\">\n" +
    "				<li class=\"col-md-3 categoryList \" data-ng-repeat=\"category in categories | orderBy: 'name': false\" >\n" +
    "					<div data-category data-resource=\"category\" ></div>\n" +
    "				</li>\n" +
    "			</ul>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "");
}]);
